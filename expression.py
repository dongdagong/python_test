#!/usr/bin/env python 

import shlex
import util 
import constants 

class Expression(object):  
   
    def __init__(self):
        if constants.debug: print util.timestamp(), 'Class [' + self.__class__.__name__ + ']' 

    def contains_operator(self, token): 
        return '=' in token or '>=' in token or '<=' in token or '>' in token or '<' in token or '<>' in token

    def infix_to_postfix(self, infix):
        util.logo() 
        
        if not (' AND ' in infix or ' OR ' in infix):
            return infix 
        # Assume the infix string do not have them in operands 
        
        precedence = {}
        precedence["AND"] = 3
        precedence["OR"] = 2
        precedence["("] = 1
        operators = []
        postfix = []
        util.log_nv('infix', infix)
        infix = infix.replace('(', ' ( ')
        infix = infix.replace(')', ' ) ')
        tokens = shlex.split(infix)
        util.log(tokens)
 
        for token in tokens:
            if self.contains_operator(token): 
                postfix.append(token)
            elif token == '(':
                operators.append(token)
            elif token == ')':
                popped = operators.pop()
                while popped != '(':
                    postfix.append(popped)
                    popped = operators.pop()
            else:
                while (not len(operators) == 0) and (precedence[operators[len(operators) - 1]] >= precedence[token]):
                    postfix.append(operators.pop())
                operators.append(token)

        while not len(operators) == 0:
            postfix.append(operators.pop())
        
        rtn = '"' + '" "'.join(postfix) + '"'
        util.log(rtn) 
        util.logc()
        return rtn 
    
if __name__ == "__main__":

    ex = Expression()
    
    print ex.infix_to_postfix('PROJECT="the hobbit"')
    print ex.infix_to_postfix('PROJECT="i, boston"')
    print ex.infix_to_postfix('PROJECT="i, boston" OR SHOT>"10" AND INTERNAL_BID>100')
    print ex.infix_to_postfix('PROJECT="the hobbit" OR SHOT>"10" AND INTERNAL_BID>100')

    