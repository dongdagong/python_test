debug = 0 

DB_FILE ='database.dat' 

C_ROWID        = 'ROWID'       
C_PROJECT      = 'PROJECT'   
C_SHOT         = 'SHOT'         
C_VERSION      = 'VERSION'      
C_STATUS       = 'STATUS'       
C_FINISH_DATE  = 'FINISH_DATE'  
C_INTERNAL_BID = 'INTERNAL_BID' 
C_CREATED_DATE = 'CREATED_DATE' 
C_IS_DELETED   = 'IS_DELETED'

C_ALL_COLUMNS = C_PROJECT + ',' + C_SHOT + ',' + C_VERSION + ',' + C_STATUS + ',' + C_FINISH_DATE + ',' + C_INTERNAL_BID + ',' + C_CREATED_DATE

GROUP_MIN = 'min'
GROUP_MAX = 'max'
GROUP_SUM = 'sum'
GROUP_COUNT = 'count'
GROUP_COLLECT = 'collect'
